---
title: About GMCServer
feature_text: |
  ## GMCServer
  A lightweight service to store and analyze data from Geiger counters
feature_image: "https://images.unsplash.com/photo-1578995511335-b54ca0772e83?auto=format&fit=crop&w=1934&q=80"
# Maybe other image to use: https://images.unsplash.com/photo-1567583789793-87f44f80ab61?auto=format&fit=crop&w=1950&q=80
excerpt: "A lightweight service to collaboratively store and analyze data from Geiger counters all over the world."
layout: page
---

GMCServer (name subject to change) is a service to store and analyze data from Geiger counters, potentially geollocated on a worldmap.

{% include button.html text="Source code" icon="github" link="https://github.com/vinceh121/gmcserver" color="#0366d6" %} {% include button.html text="Install GMCServer" link="/install" %} {% include button.html text="Use public instance" link="https://gmc.vinceh121.me" color="#0d94e7" %}

## Features

- Account management
- Secure tokens
- World map
- Mobile friendly web interface
- Free and Open Source
- Dynamic statistical analysis of data

Soon:
- Has documentation

## Use it

You can use the public instance of GMCServer [here](https://gmc.vinceh121.me)

## Installation

This section is to complete.


